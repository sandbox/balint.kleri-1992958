<?php

/**
 * @file
 * Video Recorder common functions.
 */

/**
 * The default RTMP URL for recording the video streams.
 */
define('VIDEO_RECORDER_DEFAULT_RTMP_URL', 'rtmp://localhost/dvr/');

/**
 * The default HTTP URL for fetching recorded video streams.
 */
define('VIDEO_RECORDER_DEFAULT_HTTP_URL', 'http://localhost:8134/live/dvr/');

/**
 * The default bandwidth limit when recording video.
 */
define('VIDEO_RECORDER_DEFAULT_CAMERA_BANDWIDTH', 0);

/**
 * The default compression quality when recording video.
 */
define('VIDEO_RECORDER_DEFAULT_CAMERA_COMPRESSION', 70);

/**
 * Frames per second (FPS) for uploaded videos
 */
define('VIDEO_RECORDER_DEFAULT_CAMERA_FPS', 30);

/**
 * The number of seconds between each keyframe for uploaded videos.
 */
define('VIDEO_RECORDER_DEFAULT_CAMERA_KEYFRAME', 2);

/**
 * The maximum length (in seconds) for uploaded videos.
 */
define('VIDEO_RECORDER_DEFAULT_CAMERA_LENGTH', 600);

/**
 * The height of the video recorder object in pixels.
 */
define('VIDEO_RECORDER_DEFAULT_HEIGHT', 480);

/**
 * The width of the video recorder object in pixels.
 */
define('VIDEO_RECORDER_DEFAULT_WIDTH', 640);

/**
 * The default image cache preset for generating thumbnails.
*/
define('VIDEO_RECORDER_DEFAULT_THUMBNAIL_IMAGE_STYLE', 'video_recorder_thumbnail');

/**
 * The path where thumbnail images are stored.
 */
define('VIDEO_RECORDER_THUMBNAIL_PATH', 'video_recorder');

/**
 * Registers a video ID with the recorder.
 */
function video_recorder_id_add($video_id) {
  if (!isset($_SESSION['video_recorder']['ids'])
    || !is_array($_SESSION['video_recorder']['ids'])) {
    $_SESSION['video_recorder']['ids'] = array();
  }

  $_SESSION['video_recorder']['ids'][$video_id] = TRUE;
}

/**
 * Generates a unique video identifier.
 */
function video_recorder_id_generate() {
  $id = sha1(drupal_random_bytes(128));
  video_recorder_id_add($id);
  return $id;
}

/**
 * Removes a video from the known ID list.
 */
function video_recorder_id_remove($video_id) {
  if (!empty($_SESSION['video_recorder']['ids'])) {
    unset($_SESSION['video_recorder']['ids']);
  }
}

/**
 * Verify that a video ID is legitimate.
 */
function video_recorder_id_verify($video_id) {
  return isset($_SESSION['video_recorder']['ids'][$video_id])
    && $_SESSION['video_recorder']['ids'][$video_id];
}

/**
 * Fetch the HTTP playback URL for a video.
 */
function video_recorder_playback_http_url($video_id) {
  $http_url = variable_get('video_recorder_http_url', VIDEO_RECORDER_DEFAULT_HTTP_URL);
  $url = check_plain($http_url . $video_id . '.flv');
  return $url;
}

/**
 * Fetch the RTMP playback URL for a video.
 */
function video_recorder_playback_rtmp_url($video_id) {
  $rtmp_url = variable_get('video_recorder_rtmp_url', VIDEO_RECORDER_DEFAULT_RTMP_URL);
  $url = check_plain($rtmp_url . $video_id);
  return $url;
}

/**
 * Fetches the file system path for the thumbnail image.
 */
function video_recorder_thumbnail_path($video_id) {
  $path = VIDEO_RECORDER_THUMBNAIL_PATH . "/thumb-{$video_id}.png";
  return file_build_uri($path);
}

/**
 * Deletes a file from the filesystem.
 */
function video_recorder_thumbnail_delete($video_id) {
  $path = video_recorder_thumbnail_path($video_id);
  $deleted = file_unmanaged_delete($path);
  return $deleted;
}

/**
 * Saves image data as a thumbnail.
 */
function video_recorder_thumbnail_save($video_id, $image_data) {
  $dir = file_build_uri(VIDEO_RECORDER_THUMBNAIL_PATH);
  file_prepare_directory($dir, FILE_CREATE_DIRECTORY);

  $path = video_recorder_thumbnail_path($video_id);
  $result = (0 !== file_unmanaged_save_data($image_data, $path, FILE_EXISTS_REPLACE));

  return $result;
}

/**
 * Fetches the URL for the thumbnail image.
 */
function video_recorder_thumbnail_url($video) {
  $path = video_recorder_thumbnail_path($video);
  $preset = variable_get('video_recorder_thumbnail_image_style', VIDEO_RECORDER_DEFAULT_THUMBNAIL_IMAGE_STYLE);
  $url = image_style_url($preset, $path);
  return $url;
}
